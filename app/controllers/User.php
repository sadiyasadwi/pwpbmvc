<?php

class User extends Controller {
    
    public function index()
    {
        $data['title'] = 'Login';
        $data['users'] = $this->model('UserModel');

        $this->view('login/index', $data);
    }

    public function logout()
    {
        $this->view('login/logout');
    }
}